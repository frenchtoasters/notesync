import time
import json
import logging
from watchdog.utils import WatchdogShutdown
from watchdog.tricks import Trick


class S3CommandTrick(Trick):

    """Executes upload to configured s3 bucket using boto3 in response to matched events."""
    # TODO: Add more options for s3 command here to provide all supported flags for uploading to s3
    def __init__(self, s3_bucket=None, s3_key=None, patterns=None, ignore_patterns=None,
                 ignore_directories=False, wait_for_process=False,
                 drop_during_process=False):
        super().__init__(
            patterns=patterns, ignore_patterns=ignore_patterns,
            ignore_directories=ignore_directories)
        self.s3_bucket = s3_bucket
        self.s3_key = s3_key
        self.wait_for_process = wait_for_process
        self.drop_during_process = drop_during_process
        self.process = None

    def on_any_event(self, event):
        from string import Template
        import boto3
        from botocore.exceptions import ClientError

        if self.drop_during_process and self.process and self.process.poll() is None:
            return

        if event.is_directory:
            object_type = 'directory'
        else:
            object_type = 'file'

        context = {
            'watch_src_path': event.src_path,
            'watch_dest_path': '',
            'watch_event_type': event.event_type,
            'watch_object': object_type,
        }

        if self.s3_bucket is None:
            if hasattr(event, 'dest_path'):
                context.update({'dest_path': event.dest_path})
                bucket = 'echo "${watch_event_type} ${watch_object} from ${watch_src_path} to ${watch_dest_path}"'
            else:
                bucket = 'echo "${watch_event_type} ${watch_object} ${watch_src_path}"'

        else:
            if hasattr(event, 'dest_path'):
                context.update({'watch_dest_path': event.dest_path})
            bucket = self.s3_bucket

        bucket = Template(bucket).safe_substitute(**context)
        s3 = boto3.client('s3')
        if len(self.ignore_patterns) > 0:
            import re
            patterns = re.compile(''.join(self.ignore_patterns))
            if not patterns.match(context.get('watch_src_path')):
                if context.get('watch_event_type') == "modified":
                    try:
                        # Creates files in directory for key
                        response = s3.upload_file(event.src_path, bucket, f"{self.s3_key}/{str(event.src_path).strip('./')}")
                    except ClientError as e:
                        logging.error(e)
                        return False
                    return response


class SyncShit(object):
    def __init__(self, directories: str = ".", command: str = None, patterns: str = "*", ignore_patterns: str = "",
                 ignore_directories: bool = False, recursive: bool = False, timeout: float = 1.0,
                 wait_for_process: bool = False, drop_during_process: bool = False, s3bucket: str = None,
                 s3key: str = None, action: str = "command"):
        logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        self.logger = logging.getLogger()
        self.config = {
            "patterns": patterns,
            "ignore_patterns": ignore_patterns,
            "command": command,
            "ignore_directories": ignore_directories,
            "wait_for_process": wait_for_process,
            "drop_during_process": drop_during_process,
            "timeout": timeout,
            "directories": directories,
            "recursive": recursive,
            "s3bucket": s3bucket,
            "s3key": s3key,
            "action": action
        }
        self.patterns = patterns
        self.ignore_patterns = ignore_patterns
        self.command = command
        self.ignore_directories = ignore_directories
        self.wait_for_process = wait_for_process
     
    @staticmethod   
    def observe_with(observer, event_handler, pathnames, recursive):
        """
        Single observer thread with a scheduled path and event handler.
        :param observer:
            The observer thread.
        :param event_handler:
            Event handler which will be called in response to file system events.
        :param pathnames:
            A list of pathnames to monitor.
        :param recursive:
            ``True`` if recursive; ``False`` otherwise.
        """
        for pathname in set(pathnames):
            observer.schedule(event_handler, pathname, recursive)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except WatchdogShutdown:
            observer.stop()
        observer.join()

    @staticmethod
    def parse_patterns(patterns_spec, ignore_patterns_spec, separator=';'):
        """
        Parses pattern argument specs and returns a two-tuple of
        (patterns, ignore_patterns).
        """
        patterns = patterns_spec.split(separator)
        ignore_patterns = ignore_patterns_spec.split(separator)
        if ignore_patterns == ['']:
            ignore_patterns = []
        return (patterns, ignore_patterns)

    def shell_watch(self):
        """
        Subcommand to execute shell commands in response to file system events.
        :param self.config:
            Configuration argument options.
        """
        from watchdog.tricks import ShellCommandTrick

        if not self.config.get("command"):
            self.config["command"] = None

        if self.config.get("debug_force_polling"):
            from watchdog.observers.polling import PollingObserver as Observer
        else:
            from watchdog.observers import Observer

        patterns, ignore_patterns = self.parse_patterns(self.config.get("patterns"),
                                                   self.config.get("ignore_patterns"))
        self.logger.info(f"Registering command: {self.config.get('command')}")
        handler = ShellCommandTrick(shell_command=self.config.get("command"),
                                    patterns=patterns,
                                    ignore_patterns=ignore_patterns,
                                    ignore_directories=self.config.get("ignore_directories"),
                                    wait_for_process=self.config.get("wait_for_process"),
                                    drop_during_process=self.config.get("drop_during_process"))
        observer = Observer(timeout=self.config.get("timeout"))
        self.observe_with(observer, handler, self.config.get("directories"), self.config.get("recursive"))
        self.logger.info(f"Register COMPLETE, for command: {self.config.get('command')}")

    def s3_watch(self):
        """
        Subcommand to execute shell commands in response to file system events.
        :param self.config:
            Configuration argument options.
        """
        if not self.config.get("s3bucket"):
            self.config["s3bucket"] = None

        if self.config.get("debug_force_polling"):
            from watchdog.observers.polling import PollingObserver as Observer
        else:
            from watchdog.observers import Observer

        patterns, ignore_patterns = self.parse_patterns(self.config.get("patterns"),
                                                        self.config.get("ignore_patterns"))
        self.logger.info(f"Registering s3 upload to bucket: {self.config.get('s3bucket')}")
        handler = S3CommandTrick(s3_bucket=self.config.get("s3bucket"),
                                 s3_key=self.config.get("s3key"),
                                 patterns=patterns,
                                 ignore_patterns=ignore_patterns,
                                 ignore_directories=self.config.get("ignore_directories"),
                                 wait_for_process=self.config.get("wait_for_process"),
                                 drop_during_process=self.config.get("drop_during_process"))
        observer = Observer(timeout=self.config.get("timeout"))
        self.observe_with(observer, handler, self.config.get("directories"), self.config.get("recursive"))
        self.logger.info(f"Register of s3 COMPLETE, for bucket: {self.config.get('s3bucket')}")


if __name__ == "__main__":
    config = json.loads(open("configuration.json", 'r').read())
    sync = SyncShit(**config)
    print(sync.config)
    if sync.config.get('action') == 'command':
        try:
            sync.shell_watch()
        except KeyboardInterrupt:
            print(f"Exiting via Keyboard Interrupt")
    elif sync.config.get('action') == 's3':
        try:
            sync.s3_watch()
        except KeyboardInterrupt:
            print(f"Exiting via Keyboard Interrupt")
