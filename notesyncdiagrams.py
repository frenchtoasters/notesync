from diagrams import Diagram, Cluster
from diagrams.programming.flowchart import Action, Database, Document, Inspection, PredefinedProcess

with Diagram("NoteSync"):
    with Cluster("Configuration"):
        persistent_storage = Database("Configured-CloudStor")
        storage_method = [PredefinedProcess("NFS"), PredefinedProcess("git"), PredefinedProcess("S3")]
        push_change = Action("Upload-Change")
    with Cluster("Base-Opration"):
        watch = Document("Watched-Directory")
        check_change = Inspection("Diff-Change")

    watch << check_change << push_change << watch